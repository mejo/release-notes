# translation of moreinfo.po to Russian
# Sergey Alyoshin <alyoshin.s@gmail.com>, 2009.
# Александр <antsev@tula.net>, 2011.
# Yuri Kozlov <yuray@komyakino.ru>, 2006, 2007, 2008.
# Yuri Kozlov <yuray@komyakino.ru>, 2009, 2011, 2013.
# Lev Lamberov <l.lamberov@gmail.com>, 2015.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2017-06-15 11:47+0500\n"
"PO-Revision-Date: 2017-06-15 13:06+0500\n"
"Last-Translator: Lev Lamberov <l.lamberov@gmail.com>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.11\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. type: Attribute 'lang' of: <chapter>
#: en/moreinfo.dbk:8
msgid "en"
msgstr "ru"

#. type: Content of: <chapter><title>
#: en/moreinfo.dbk:9
msgid "More information on &debian;"
msgstr "Дополнительная информация о &debian;"

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:11
msgid "Further reading"
msgstr "Что ещё можно прочитать"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:13
msgid ""
"Beyond these release notes and the installation guide, further documentation "
"on Debian is available from the Debian Documentation Project (DDP), whose "
"goal is to create high-quality documentation for Debian users and "
"developers, such as the Debian Reference, Debian New Maintainers Guide, the "
"Debian FAQ, and many more.  For full details of the existing resources see "
"the <ulink url=\"&url-ddp;\">Debian Documentation website</ulink> and the "
"<ulink url=\"&url-wiki;\">Debian Wiki</ulink>."
msgstr ""
"Помимо этой информации о выпуске и руководства по установке, существует "
"документация по Debian, разрабатываемая проектом документирования Debian "
"(DDP), целью которого является создание высококачественной документации для "
"пользователей и разработчиков Debian. В состав этой документации входит "
"справочник Debian, руководство нового сопровождающего Debian, ЧаВО по Debian "
"и многое другое. Полную информацию о доступной документации можно посмотреть "
"на <ulink url=\"&url-ddp;\">веб-сайте проекта документирования Debian</"
"ulink> и <ulink url=\"&url-wiki;\">Debian Wiki</ulink>."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:23
msgid ""
"Documentation for individual packages is installed into <filename>/usr/share/"
"doc/<replaceable>package</replaceable></filename>.  This may include "
"copyright information, Debian specific details, and any upstream "
"documentation."
msgstr ""
"Документация по конкретным пакетам устанавливается в каталог <filename>/usr/"
"share/doc/<replaceable>пакет</replaceable></filename>. Там может находиться "
"информация об авторских правах, специфичная для Debian информация и "
"документация из основной ветки разработки."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:31
msgid "Getting help"
msgstr "Если нужна помощь"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:33
msgid ""
"There are many sources of help, advice, and support for Debian users, though "
"these should only be considered after researching the issue in available "
"documentation.  This section provides a short introduction to these sources "
"which may be helpful for new Debian users."
msgstr ""
"Пользователи Debian могут воспользоваться помощью, советами и поддержкой из "
"разных источников, но к ним следует прибегать только в том случае, если вам "
"не удалось найти ответа в доступной документации. Данный раздел содержит "
"краткое описание дополнительных источников помощи, которые могут оказаться "
"полезными для новых пользователей Debian."

#. type: Content of: <chapter><section><section><title>
#: en/moreinfo.dbk:39
msgid "Mailing lists"
msgstr "Списки рассылки"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:41
msgid ""
"The mailing lists of most interest to Debian users are the debian-user list "
"(English) and other debian-user-<replaceable>language</replaceable> lists "
"(for other languages).  For information on these lists and details of how to "
"subscribe see <ulink url=\"&url-debian-list-archives;\"></ulink>.  Please "
"check the archives for answers to your question prior to posting and also "
"adhere to standard list etiquette."
msgstr ""
"Основной интерес для пользователей Debian представляют англоязычный список "
"рассылки debian-user, или списки debian-user-<replaceable>язык</replaceable> "
"и debian-<replaceable>язык</replaceable> для других языков. Для русского "
"языка это список debian-russian. Информацию о списках рассылки и о том как "
"на них подписаться см. на <ulink url=\"&url-debian-list-archives;\"></"
"ulink>. Пожалуйста, перед тем, как отправить вопрос в список рассылки, "
"поищите ответ на него в архивах. Просим также придерживаться общепринятых "
"норм почтового этикета."

#. type: Content of: <chapter><section><section><title>
#: en/moreinfo.dbk:51
msgid "Internet Relay Chat"
msgstr "IRC"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:53
msgid ""
"Debian has an IRC channel dedicated to support and aid for Debian users, "
"located on the OFTC IRC network.  To access the channel, point your favorite "
"IRC client at irc.debian.org and join <literal>#debian</literal>."
msgstr ""
"Для поддержки пользователей Debian есть IRC-канал, размещённый в IRC-сети "
"OFTC. Чтобы войти на канал, соединитесь с сервером irc.debian.org с помощью "
"своего любимого IRC-клиента и присоединитесь к каналу <literal>#debian</"
"literal>. Для русскоязычных пользователей существует канал <literal>#debian-"
"russian</literal> в той же сети."

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:58
msgid ""
"Please follow the channel guidelines, respecting other users fully.  The "
"guidelines are available at the <ulink url=\"&url-wiki;DebianIRC\">Debian "
"Wiki</ulink>."
msgstr ""
"Просим вас следовать правилам поведения на канале и уважать других "
"пользователей. Правила поведения на канале описаны в <ulink url=\"&url-wiki;"
"DebianIRC\">вики Debian</ulink>."

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:63
msgid ""
"For more information on OFTC please visit the <ulink url=\"&url-irc-host;"
"\">website</ulink>."
msgstr ""
"Более подробную информацию об OFTC можно получить на <ulink url=\"&url-irc-"
"host;\">веб-сайте сети</ulink>."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:71
msgid "Reporting bugs"
msgstr "Как сообщить об ошибке"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:73
msgid ""
"We strive to make Debian a high-quality operating system; however that does "
"not mean that the packages we provide are totally free of bugs.  Consistent "
"with Debian's <quote>open development</quote> philosophy and as a service to "
"our users, we provide all the information on reported bugs at our own Bug "
"Tracking System (BTS).  The BTS can be browsed at <ulink url=\"&url-bts;\"></"
"ulink>."
msgstr ""
"Мы приложили немало усилий, чтобы сделать Debian операционной системой "
"высокого качества, однако это не означает, что поставляемые нами пакеты "
"совсем не содержат никаких ошибок. Такой подход согласуется с философией "
"<quote>открытой разработки</quote> Debian. Мы предоставляем нашим "
"пользователям полную информацию обо всех обнаруженных ошибках с помощью "
"нашей системы отслеживания ошибок (BTS). Вы можете обратиться к ней по "
"адресу <ulink url=\"&url-bts;\"></ulink>."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:81
msgid ""
"If you find a bug in the distribution or in packaged software that is part "
"of it, please report it so that it can be properly fixed for future "
"releases.  Reporting bugs requires a valid e-mail address.  We ask for this "
"so that we can trace bugs and developers can get in contact with submitters "
"should additional information be needed."
msgstr ""
"Если вы обнаружите ошибку в дистрибутиве или каком-то из его пакетов, "
"пожалуйста, сообщите о ней, чтобы в будущих выпусках она была исправлена. "
"Чтобы сообщить об ошибке, требуется рабочий адрес электронной почты. Это "
"необходимо для того, чтобы мы могли отслеживать ошибки, а разработчики могли "
"связываться с отправителями отчётов об ошибках, если им понадобится "
"дополнительная информация."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:88
msgid ""
"You can submit a bug report using the program <command>reportbug</command> "
"or manually using e-mail.  You can find out more about the Bug Tracking "
"System and how to use it by reading the reference documentation (available "
"at <filename>/usr/share/doc/debian</filename> if you have <systemitem role="
"\"package\">doc-debian</systemitem> installed) or online at the <ulink url="
"\"&url-bts;\">Bug Tracking System</ulink>."
msgstr ""
"Отправить сообщение об ошибке можно с помощью программы <command>reportbug</"
"command> или вручную по электронной почте. Более подробную информацию о "
"системе отслеживания ошибок и о том, как её использовать, можно прочитать в "
"справочной документации (она доступна в каталоге <filename>/usr/share/doc/"
"debian</filename> после установки пакета <systemitem role=\"package\">doc-"
"debian</systemitem>) или на сайте <ulink url=\"&url-bts;\">системы "
"отслеживания ошибок</ulink>."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:98
msgid "Contributing to Debian"
msgstr "Как помочь Debian"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:100
msgid ""
"You do not need to be an expert to contribute to Debian.  By assisting users "
"with problems on the various user support <ulink url=\"&url-debian-list-"
"archives;\">lists</ulink> you are contributing to the community.  "
"Identifying (and also solving) problems related to the development of the "
"distribution by participating on the development <ulink url=\"&url-debian-"
"list-archives;\">lists</ulink> is also extremely helpful.  To maintain "
"Debian's high-quality distribution, <ulink url=\"&url-bts;\">submit bugs</"
"ulink> and help developers track them down and fix them.  The tool "
"<systemitem role=\"package\">how-can-i-help</systemitem> helps you to find "
"suitable reported bugs to work on.  If you have a way with words then you "
"may want to contribute more actively by helping to write <ulink url=\"&url-"
"ddp-svn-info;\">documentation</ulink> or <ulink url=\"&url-debian-i18n;"
"\">translate</ulink> existing documentation into your own language."
msgstr ""
"Чтобы помочь Debian, не нужно быть экспертом. Помогая пользователям в "
"решении их проблем в <ulink url=\"&url-debian-list-archives;\">списках "
"рассылки</ulink>, вы уже помогаете сообществу. Выявление (а также решение) "
"проблем, связанных с разработкой дистрибутива, участие в обсуждениях в "
"<ulink url=\"&url-debian-list-archives;\">списках рассылки для "
"разработчиков</ulink>, также весьма важно. Чтобы помочь поддержать высокое "
"качество Debian, <ulink url=\"&url-bts;\">отправляйте сообщения об ошибках</"
"ulink> и помогайте разработчикам отслеживать и исправлять их. Программа "
"<systemitem role=\"package\">how-can-i-help</systemitem> поможет вам найти "
"подходящие ошибки из системы отслеживания ошибок, над которыми вы можете "
"поработать. Если вы хорошо владеете языком, то можете более активно помочь в "
"написании <ulink url=\"&url-ddp-vcs-info;\">документации</ulink> или <ulink "
"url=\"&url-debian-i18n;\">переводе</ulink> существующей документации на ваш "
"родной язык."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:117
msgid ""
"If you can dedicate more time, you could manage a piece of the Free Software "
"collection within Debian.  Especially helpful is if people adopt or maintain "
"items that people have requested for inclusion within Debian.  The <ulink "
"url=\"&url-wnpp;\">Work Needing and Prospective Packages database</ulink> "
"details this information.  If you have an interest in specific groups then "
"you may find enjoyment in contributing to some of Debian's <ulink url=\"&url-"
"debian-projects;\">subprojects</ulink> which include ports to particular "
"architectures and <ulink url=\"&url-debian-blends;\">Debian Pure Blends</"
"ulink> for specific user groups, among many others."
msgstr ""
"Если вы можете уделить Debian больше времени, можно взяться за сопровождение "
"какой-нибудь свободной программы Free Software. В частности, полезной была "
"бы поддержка пакетов, которые кто-то особенно хотел бы видеть в Debian. "
"Подробную информацию можно найти на странице <ulink url=\"&url-wnpp;"
"\">требующих доработки и планируемых пакетов</ulink>. Если вам интересна "
"какая-то специфическая группа пользователей, возможно, вы захотите "
"участвовать в одном из <ulink url=\"&url-debian-projects;\">дочерних "
"проектов</ulink> Debian. Среди них перенос Debian на другие архитектуры и "
"<ulink url=\"&url-debian-blends;\">Debian Pure Blends</ulink>."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:128
msgid ""
"In any case, if you are working in the free software community in any way, "
"as a user, programmer, writer, or translator you are already helping the "
"free software effort.  Contributing is rewarding and fun, and as well as "
"allowing you to meet new people it gives you that warm fuzzy feeling inside."
msgstr ""
"В любом случае, если вы участвуете в работе сообщества Свободного ПО в "
"качестве пользователя, программиста, писателя или переводчика, вы уже "
"помогаете Свободному программному обеспечению. Такое участие приносит "
"удовольствие и радость, а также даёт возможность познакомиться с новыми "
"людьми и придаёт вам те самые тёплые чувства и переживания."
