<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
  "https://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % languagedata SYSTEM "language.ent" > %languagedata;
  <!ENTITY % shareddata   SYSTEM "../release-notes.ent" > %shareddata;
]>

<chapter id="ch-information" lang="en">
<title>Issues to be aware of for &releasename;</title>

<para>
Sometimes, changes introduced in a new release have side-effects
we cannot reasonably avoid, or they expose
bugs somewhere else. This section documents issues we are aware of.  Please also
read the errata, the relevant packages' documentation, bug reports, and other
information mentioned in <xref linkend="morereading"/>.
</para>

<section id="upgrade-specific-issues">
  <title>Upgrade specific items for &releasename;</title>
  <para>
    This section covers items related to the upgrade from
    &oldreleasename; to &releasename;.
  </para>

  <section id="isa-baseline-for-s390x" arch="s390x">
    <!-- stretch to buster-->
    <title>s390x ISA raised to z196</title>
    <para>
      The baseline ISA of the <literal>s390x</literal> port has been raised
      to z196. This enables Debian to support packages like <systemitem
      role="package">rust</systemitem>, <systemitem
      role="package">go</systemitem> and <systemitem
      role="package">nodejs</systemitem>, which do not support older ISAs.
    </para>
    <para>
      As a consequence people should not upgrade to buster if they have an
      older CPU.
    </para>
  </section>

  <section id="hidepid-unsupported">
    <!-- stretch to buster-->
    <title>Hidepid mount option for procfs unsupported</title>
    <para>
      Using the <literal>hidepid</literal> mount option for
      <filename>/proc</filename> is known to cause problems with current
      versions of systemd, and is considered by systemd upstream to be an
      unsupported configuration. Users who have modified
      <filename>/etc/fstab</filename> to enable this option are advised to
      disable it before the upgrade, to ensure login sessions work on
      &releasename;. (A possible route to re-enabling it is outlined on the
      wiki's <ulink
      url="&url-wiki;Hardening#Mounting_.2Fproc_with_hidepid">Hardening</ulink>
      page.)
    </para>
  </section>

  <section id="ypbind-defaults">
    <!-- stretch to buster-->
    <title>ypbind fails to start with -no-dbus</title>
    <para>
      The default options of <systemitem role="package">ypbind</systemitem>
      have changed. However, if you have modified this file the old default
      will not be updated and you must make sure that the
      <literal>YPBINDARGS=</literal> option in
      <filename>/etc/default/nis</filename> does not include
      <literal>-no-dbus</literal>. With <literal>-no-dbus</literal> present,
      <command>ypbind</command> will fail to start, and you may not be able to
      log in. For more info see <ulink url="&url-bts;906436">bug
      #906436</ulink>.
    </para>
  </section>

  <section>
    <!-- stretch to buster -->
    <title>sshd fails to authenticate</title>
    <para>
      The semantics of <literal>PubkeyAcceptedKeyTypes</literal> and the
      similar <literal>HostbasedAcceptedKeyTypes</literal> options for
      <systemitem role="package">sshd</systemitem> have changed. These now
      specify signature algorithms that are accepted for their respective
      authentication mechanism, where previously they specified accepted key
      types. This distinction matters when using the RSA/SHA2 signature
      algorithms <literal>rsa-sha2-256</literal>,
      <literal>rsa-sha2-512</literal> and their certificate counterparts.
      Configurations that override these options but omit these algorithm names
      may cause unexpected authentication failures.
    </para>
    <para>
      No action is required for configurations that accept the default for
      these options.
    </para>
  </section>

  <section id="entropy-starvation">
    <!-- stretch to buster -->
    <title>Daemons fail to start or system appears to hang during boot</title>
    <para>
      Due to <systemitem role="package">systemd</systemitem> needing entropy
      during boot and the kernel treating such calls as blocking when available
      entropy is low, the system may hang for minutes to hours until the
      randomness subsystem is sufficiently initialized (<literal>random: crng
      init done</literal>). For <literal>amd64</literal> systems supporting the
      <literal>RDRAND</literal> instruction this issue is avoided by the
      Debian kernel using this instruction by default
      (<literal>CONFIG_RANDOM_TRUST_CPU</literal>).
    </para>
    <para>
      Non-<literal>amd64</literal> systems and some types of virtual machines
      need to provide a different source of entropy to continue fast booting.
      <systemitem role="package">haveged</systemitem> has been chosen for this
      within the Debian Installer project and may be a valid option if hardware
      entropy is not available on the system. On virtual machines consider
      forwarding entropy from the host to the VMs via
      <literal>virtio_rng</literal>.
    </para>
    <para>
      If you read this after upgrading a remote system to &releasename;,
      ping the system on the network continuously as this adds entropy
      to the randomness pool and the system will eventually be reachable
      by ssh again.
    </para>
    <para>
      See <ulink url="&url-bts;916690">bug #916690</ulink> for details and
      <ulink url="https://daniel-lange.com/archives/152-hello-buster.html">
      DLange's overview of the issue</ulink> for other options.
    </para>
  </section>
     
  <section id="migrate-interface-names">
    <!-- (jessie to) stretch to buster -->
    <title>Migrating from legacy network interface names</title>
    <para>
     If your system was upgraded from an earlier release, and still uses
     the old-style network interface names that were deprecated with
     stretch (such as <literal>eth0</literal> or <literal>wlan0</literal>),
     you should be aware that <systemitem role="package">udev</systemitem>
     in buster no longer supports the mechanism of defining their names via
     <filename>/etc/udev/rules.d/70-persistent-net.rules</filename>. To
     avoid the danger of your machine losing networking after the upgrade
     to buster, it is recommended that you migrate in advance to the new
     naming scheme (usually meaning names like <literal>enp0s1</literal> or
     <literal>wlp2s5</literal>, which incorporate PCI bus- and
     slot-numbers). Take care to update any interface names hard-coded in
     configuration for firewalls, <systemitem role="package">ifupdown</systemitem>,
     and so on.
    </para>
    <para>
     The alternative is to switch to a supported mechanism for enforcing
     the old naming scheme, such as the <literal>net.ifname=0</literal>
     kernel commandline option or a systemd <filename>.link</filename>
     file (see <ulink
     url="https://manpages.debian.org/systemd.link">systemd.link(5)</ulink>).
    </para>
    <para>
     To find the new-style names that will be used, first find the
     current names of the relevant interfaces:
    </para>
    <screen>
$ echo /sys/class/net/[ew]*
    </screen>
    <para>
      For each of these names, check whether it is used in configuration
      files, and what name <systemitem role="package">udev</systemitem>
      would prefer to use for it:
    </para>
    <screen>
$ sudo rgrep -w <replaceable>eth0</replaceable> /etc
$ udevadm test-builtin net_id /sys/class/net/<replaceable>eth0</replaceable> 2>/dev/null
    </screen>
    <para>
      This should give enough information to devise a migration plan. (If the
      <literal>udevadm</literal> output includes an <quote>onboard</quote>
      name, that takes priority; MAC-based names are normally treated as a
      fallback, but may be needed for USB network hardware.)
    </para>
    <para>
      Once you are ready to carry out the switch, disable
      <filename>70-persistent-net.rules</filename> either by renaming it or by
      commenting out individual lines. On virtual machines you will need to remove
      the files <filename>/etc/systemd/network/99-default.link</filename> and
      (if using virtio network devices)
      <filename>/etc/systemd/network/50-virtio-kernel-names.link</filename>.
      Then rebuild the <filename>initrd</filename>:
    </para>
    <screen>
$ sudo update-initramfs -u
    </screen>
    <para>
     and reboot. Your system should now have new-style network interface
     names. Adjust any remaining configuration files, and test your system.
    </para>
    <para>
     See the
     <ulink url="https://www.freedesktop.org/wiki/Software/systemd/PredictableNetworkInterfaceNames/">upstream
     documentation</ulink> and the <literal>udev</literal>
     <filename>README.Debian</filename> for further information.
    </para>
  </section>

  <section id="openssl-defaults">
    <!-- stretch to buster -->
    <title>OpenSSL default version and security level raised</title>
    <para>
      Following various security recommendations, the default minimum TLS
      version has been changed from TLSv1 to TLSv1.2.
    </para>
    <para>
      The default security level for TLS connections has also been increased from
      level 1 to level 2. This moves from the 80 bit security level to the 112
      bit security level and will require 2048 bit or larger RSA and DHE keys,
      224 bit or larger ECC keys, and SHA-2.
    </para>
    <para>
      The system wide settings can be changed in
      <filename>/etc/ssl/openssl.cnf</filename>. Applications might also have
      an application specific way to override the defaults.
    </para>
    <para>
      In the default <filename>/etc/ssl/openssl.cnf</filename> there is a
      <literal>MinProtocol</literal> and <literal>CipherString</literal>
      line. The <literal>CipherString</literal> can also set the security
      level. Information about the security levels can be found in the <ulink
      url="https://manpages.debian.org/SSL_CTX_set_security_level(3ssl)">SSL_CTX_set_security_level(3ssl)</ulink>
      manpage. The list of valid strings for the minimum protocol version can
      be found in <ulink
      url="https://manpages.debian.org/SSL_CONF_cmd(3ssl)">SSL_CONF_cmd(3ssl)</ulink>. Other
      information can be found in <ulink
      url="https://manpages.debian.org/ciphers(1ssl)">ciphers(1ssl)</ulink> and
      <ulink
      url="https://manpages.debian.org/config(5ssl)">config(5ssl)</ulink>.
    </para>
    <para>
      Changing the system wide defaults in <filename>/etc/ssl/openssl.cnf</filename>
      back to their previous values can be done by setting:
      <programlisting>
        MinProtocol = None
        CipherString = DEFAULT
      </programlisting>
    </para>
    <para>
      It's recommended that you contact the remote site if the defaults
      cause problems.
    </para>
  </section>

  <section id="apps-in-gnome-on-wayland">
    <title>Some applications don't work in GNOME on Wayland</title>
    <para>
      GNOME in buster has changed its default display server from Xorg to
      Wayland. Some applications, including the
      popular package manager <systemitem role="package">synaptic</systemitem>,
      the default Simplified Chinese input method,
      <systemitem role="package">fcitx</systemitem>,
      and most screen recording applications, have not been updated to work
      properly under Wayland. In order to use these packages,
      one needs to log in with a <literal>GNOME on Xorg</literal> session.
    </para>
  </section>

  <section id="noteworthy-obsolete-packages" condition="fixme">
    <title>Noteworthy obsolete packages</title>
    <para>
      The following is a list of known and noteworthy obsolete
      packages (see <xref linkend="obsolete"/> for a description).
      <programlisting condition="fixme">
        TODO: Use the change-release information and sort by popcon

         This needs to be reviewed based on real upgrade logs (jfs)

         Alternative, another source of information is the UDD
         'not-in-testing' page:
         https://udd.debian.org/bapase.cgi?t=testing
      </programlisting>
    </para>
    <para>
      The list of obsolete packages includes:
      <itemizedlist>
        <listitem>
          <para>
            The package <systemitem role="package">mcelog</systemitem> is no
            longer supported with kernel versions above 4.12. <systemitem
            role="package">rasdaemon</systemitem> can be used as its
            replacement.
          </para>
        </listitem>
        <listitem>
          <para>
            The package <systemitem role="package">revelation</systemitem>,
            which is used to store passwords, is not included in &releasename;.
            <systemitem role="package">keepass2</systemitem> can import
            previously exported password XML files from <systemitem
            role="package">revelation</systemitem>. Please make sure you export
            your data from revelation before upgrading, to avoid losing access
            to your passwords.
          </para>
        </listitem>
        <listitem>
          <para>
            The package <systemitem role="package">phpmyadmin</systemitem>
            is not included in &releasename;.
          </para>
        </listitem>
        <listitem>
          <para>
            <systemitem role="package">ipsec-tools</systemitem> and <systemitem
            role="package">racoon</systemitem> have been removed from buster as
            their source has been lagging behind in adapting to new threats.
          </para>
          <para>
            Users are encouraged to migrate to <systemitem
            role="package">libreswan</systemitem>, which has broader protocol
            compatibility and is being actively maintained upstream.
          </para>
          <para>
            <systemitem role="package">libreswan</systemitem> should be fully
            compatible in terms of communication protocols since it implements
            a superset of <systemitem role="package">racoon</systemitem>'s
            supported protocols.
          </para>
        </listitem>
        <listitem>
          <para>
            The simple MTA <systemitem role="package">ssmtp</systemitem>
            has been dropped for &releasename;. This is due to it currently
            not validating TLS certs; see <ulink url="&url-bts;662960">bug
            #662960</ulink>.
          </para>
        </listitem>
        <listitem>
          <para>
            The <systemitem role="package">ecryptfs-utils</systemitem> package
            is not part of buster due to an unfixed serious bug (<ulink
            url="&url-bts;765854">#765854</ulink>). At the time of writing this
            paragraph, there was no clear advice for users of eCryptfs,
            except not to upgrade.
          </para>
        </listitem>
      </itemizedlist>
    </para>
  </section>
  <section id="deprecated-components" condition="fixme">
    <title>Deprecated components for &releasename;</title>
    <para>
      With the next release of &debian; &nextrelease; (codenamed
      &nextreleasename;) some features will be deprecated. Users
      will need to migrate to other alternatives to prevent
      trouble when updating to &debian; &nextrelease;.
    </para>
    <para>
      This includes the following features:
    </para>

    <itemizedlist>
      <listitem>
        <para>
          Python 2 will stop being supported by its upstream on <ulink
          url="https://www.python.org/dev/peps/pep-0373/">January 1,
          2020</ulink>. &debian; hopes to drop <systemitem
          role="package">python-2.7</systemitem> for &nextrelease;. If users
          have functionality that relies on <command>python</command>, they
          should prepare to migrate to <command>python3</command>.
        </para>
      </listitem>
      <listitem>
        <para>
          Icinga 1.x is EOL upstream since 2018-12-31; while the
          <systemitem role="package">icinga</systemitem> package
          is still present, users should use the buster lifetime
          to migrate to Icinga 2
          (<systemitem role="package">icinga2</systemitem> package)
          and Icinga Web 2
          (<systemitem role="package">icingaweb2</systemitem>
          package). The
          <systemitem role="package">icinga2-classicui</systemitem>
          package is still present to use the Icinga 1.x CGI web
          interface with Icinga 2, but the support for it will be
          removed in Icinga 2.11. Icinga Web 2 should be used
          instead.
        </para>
      </listitem>
      <listitem>
        <para>
          The Mailman mailing list manager suite version 3 is newly available
          in this release. Mailman has been split up into various components;
          the core is available in the package <systemitem
          role="package">mailman3</systemitem> and the full suite can be
          obtained via the <systemitem
          role="package">mailman3-full</systemitem> metapackage.
        </para>
        <para>
          The legacy Mailman version 2.1 remains available in this release in
          the package <systemitem role="package">mailman</systemitem>, so you
          can migrate any existing installations at your own pace.  The Mailman
          2.1 package will be kept in working order for the foreseeable future,
          but will not see any major changes or improvements. It will be
          removed from the first Debian release after Mailman upstream has
          stopped support for this branch.
        </para>
        <para>
          Everyone is encouraged to upgrade to Mailman 3, the modern release
          under active development.
        </para>
      </listitem>
      <listitem>
        <para>
          The packages <systemitem role="package">spf-milter-python</systemitem>
          and <systemitem role="package">dkim-milter-python</systemitem> are no longer
          actively developed upstream, but their more feature-rich replacements,
          <systemitem role="package">pyspf-milter</systemitem> and <systemitem
          role="package">dkimpy-milter</systemitem>, are available in buster. Users
          should migrate to the new packages before the old ones are removed in bullseye.
        </para>
      </listitem>

    </itemizedlist>
  </section>
  <section id="before-first-reboot">
    <title>Things to do post upgrade before rebooting</title>
    <!-- If there is nothing to do -->
    <para>
      When <literal>apt full-upgrade</literal> has finished, the
      <quote>formal</quote> upgrade is complete.  For the upgrade to
      &releasename;, there are no special actions needed before
      performing a reboot.
    </para>
    <!-- If there is something to do -->
    <para condition="fixme">
      When <literal>apt full-upgrade</literal> has finished, the <quote>formal</quote> upgrade
      is complete, but there are some other things that should be taken care of
      <emphasis>before</emphasis> the next reboot.
    </para>

    <programlisting condition="fixme">
      add list of items here
      <!--itemizedlist>
        <listitem>
          <para>
            bla bla blah
          </para>
        </listitem>
      </itemizedlist-->
    </programlisting>

  </section>
  <section id="obsolete-sysvinit-packages">
    <!-- stretch to buster -->
    <title>SysV init related packages no longer required</title>
    <note>
      <para>
        This section does not apply if you have decided to stick with sysvinit-core.
      </para>
    </note>
    <para>
      After the switch to systemd as default init system in Jessie and further refinements
      in Stretch, various SysV related packages are no longer required and can now be
      purged safely via
      <screen>apt purge initscripts sysv-rc insserv startpar</screen>
    </para>
  </section>
</section>

<section id="limited-security-support">
  <title>Limitations in security support</title>
  <para>
    There are some packages where Debian cannot promise to provide
    minimal backports for security issues.  These are covered in the
    following subsections.
  </para>
  <note>
    <para>
      The package <systemitem
      role="package">debian-security-support</systemitem> helps to track the
      security support status of installed packages.
    </para>
  </note>

  <section id="browser-security" condition="fixme">
    <!-- keep as a regular (?) -->
    <title>Security status of web browsers and their rendering engines</title>
    <para>
      Debian &release; includes several browser engines which are affected by a
      steady stream of security vulnerabilities. The high rate of
      vulnerabilities and partial lack of upstream support in the form of long
      term branches make it very difficult to support these browsers and
      engines with backported security fixes.  Additionally, library
      interdependencies make it extremely difficult to update to newer upstream
      releases. Therefore, browsers built upon e.g. the webkit and khtml
      engines<footnote><para>These engines are shipped in a number of different
      source packages and the concern applies to all packages shipping
      them. The concern also extends to web rendering engines not explicitly
      mentioned here, with the exception of <systemitem
      role="source">webkit2gtk</systemitem>.</para></footnote> are included in
      &releasename;, but not
      covered by security support. These browsers should not be used against
      untrusted websites.
      The <systemitem role="source">webkit2gtk</systemitem> source package is
      covered by security support.
    </para>
    <para>
      For general web browser use we recommend Firefox or Chromium.
      They will
      be kept up-to-date by rebuilding the current ESR releases for
      stable.
      The same strategy will be applied for Thunderbird.
    </para>
  </section>

  <section id="golang-static-linking">
    <!-- stretch to buster -->
    <title>Go based packages</title>
    <para>
      The Debian infrastructure currently doesn't properly enable rebuilding
      packages that statically link parts of other packages on a large
      scale. Until buster that hasn't been a problem in practice, but with the
      growth of the Go ecosystem it means that Go based packages won't be
      covered by regular security support until the infrastructure is improved
      to deal with them maintainably.
    </para>
    <para>
      If updates are warranted, they can only come via regular point releases,
      which may be slow in arriving.
    </para>
  </section>
</section>

<section id="package-specific-issues">
  <title>Package specific issues</title>
  <para>
    In most cases, packages should upgrade smoothly between
    &oldreleasename; and &releasename;.  There are a small number of
    cases where some intervention may be required, either before or
    during the upgrade; these are detailed below on a per-package
    basis.
  </para>

  <section id="glibc-and-linux" arch="i386;amd64">
    <!-- stretch to buster-->
    <title>Glibc requires Linux kernel 3.2 or higher</title>
    <para>
      Starting with <systemitem role="source">glibc</systemitem> 2.26, Linux
      kernel 3.2 or later is required. To avoid completely breaking the system,
      the preinst for <systemitem role="package">libc6</systemitem> performs a
      check. If this fails, it will abort the package installation, which will
      leave the upgrade unfinished.  If the system is running a kernel older
      than 3.2, please update it before starting the distribution upgrade.
    </para>
  </section>

  <section id="su-environment-variables">
    <!-- stretch to buster-->
    <title>Semantics for using environment variables for su changed</title>
    <para>
      <literal>su</literal> has changed semantics in &releasename; and no longer
      preserves the user environment variables <literal>DISPLAY</literal> and
      <literal>XAUTHORITY</literal>. If you need to run graphical applications
      with <literal>su</literal>, you will have to explicitly set them to allow
      access to your display. See <ulink
      url="&url-bts;905409">bug #905409</ulink> for an extensive discussion.
    </para>
  </section>

  <section id="postgresql-reindex">
    <!-- stretch to buster-->
    <title>Existing PostgreSQL databases need to be reindexed</title>
    <para>
      When upgrading from stretch to buster, the <systemitem
      role="source">glibc</systemitem> locale data is upgraded.  Specifically,
      this changes how PostgreSQL sorts data in text indexes.  To avoid
      corruption, such indexes need to be <literal>REINDEX</literal>ed
      immediately after upgrading the <systemitem
      role="package">locales</systemitem> or <systemitem
      role="package">locales-all</systemitem> packages, before putting the
      database back into production.
    </para>
    <para>
      Suggested command: <screen>sudo -u postgres reindexdb --all</screen>
    </para>
    <para>
      Alternatively, upgrade the databases to PostgreSQL 11 using
      <command>pg_upgradecluster</command>.
      (This uses <command>pg_dump</command> by default which will rebuild all
      indexes. Using <literal>-m upgrade</literal> or <command>pg_upgrade</command>
      is <emphasis>not</emphasis> safe because it preserves the now-wrong
      index ordering.)
    </para>
    <para>
      Refer to the <ulink url="https://wiki.postgresql.org/wiki/Locale_data_changes">PostgreSQL Wiki</ulink>
      for more information.
    </para>
  </section>

  <section id="mutt-and-neomutt">
    <!-- stretch to buster -->
    <title>mutt and neomutt</title>
    <para>
      In stretch, the package <systemitem role="package">mutt</systemitem>
      had patches applied from the sources at <ulink
      url="https://neomutt.org">https://neomutt.org</ulink>. Starting
      from buster, the package providing <literal>/usr/bin/mutt</literal> will
      instead be purely based on the original sources from <ulink
      url="http://www.mutt.org">http://www.mutt.org</ulink>, and a separate
      <systemitem role="package">neomutt</systemitem> package is available
      providing <literal>/usr/bin/neomutt</literal>.
    </para>
    <para>
      This means that some of the features that were previously provided by
      <systemitem role="package">mutt</systemitem> are no longer available. If
      this breaks your configuration you can install <systemitem
      role="package">neomutt</systemitem> instead.
    </para>
  </section>

</section>

</chapter>
